<?php
/**
 * Created by Vlad Doroshchuk.
 */
require_once $_SERVER['DOCUMENT_ROOT']."/app/core/Controller.php";
require_once $_SERVER['DOCUMENT_ROOT']."/app/models/MapModel.php";

class MapController extends Controller{
    private $model;

    public function __construct($controller, $action){
        $this->includes = array (
            'css' => array ('style.css'),
            'js' => array('unit.js')
        );
        parent::__construct($controller, $action, $this->includes);
    }

     /** render map with units */
    public function index(){
        $model = new MapModel();
        $this->setView($model, $this->includes);
        $this->model = $model;
        $this->view->render(array('model' => $model));
    }

}