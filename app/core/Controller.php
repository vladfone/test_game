<?php
/**
 * Created by Vlad Doroshchuk.
 */
/** Main class controller
    $controller - controller name
    $action - action name
    $includes - array includes(js, css)
 */
class Controller {
    protected $controller;
    protected $action;
    protected $includes;

    public function __construct($controller, $action, $includes){
        require_once $_SERVER['DOCUMENT_ROOT']."/app/core/View.php";
        $this->action = $action;
        $this->controller = $controller;
        $this->view = new View(array(),$includes, $controller);
    }
    /** initialise View object */
    protected function setView($model, $includes){
        $this->view = new View($model, $includes, $this->controller);
    }

    /** return id from $_GET */
    protected static function parseId(){
        $id = explode("/", $_GET['id']);
        if (isset($id[count($id)-1]) && $id[count($id)-1] > 0)
            $id = $id[count($id)-1];
        else
            $id = null;
        return $id;
    }
}