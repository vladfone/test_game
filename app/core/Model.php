<?php
/**
 * Created by Vlad Doroshchuk.
 */
require_once $_SERVER['DOCUMENT_ROOT']."/app/config/map.php";

/** Main class model
*/
class Model {
    /**
     * texturies
     */
    const MOUNTAIN = 1;
    const WATER = 2;
    const FOREST = 3;
    const EARTH = 4;
    const SAND = 5;
    const BOG = 6;

    /**
     * units
     */
    const BASE_TEAM_1 = 100;
    const BASE = 200;
    const BASE_TEAM_2 = 300;
    const INFANTRY_TEAM_1 = 101;
    const INFANTRY = 201;
    const INFANTRY_TEAM_2 = 301;
    const TANKS_TEAM_1 = 102;
    const TANKS = 202;
    const TANKS_TEAM_2 = 302;
    const WARPLANES_TEAM_1 = 103;
    const WARPLANES = 203;
    const WARPLANES_TEAM_2 = 303;

    /**
     * directions of move
     */
    const DIRECTION_LEFT = 1;
    const DIRECTION_RIGHT = 2;
    const DIRECTION_UP = 3;
    const DIRECTION_DOWN = 4;

    const PLACE_FOR_BASES = 5;

    public $map = array();

    public $unit = array();

    protected $_heightMap = MAP_HEIGHT;
    protected $_widthMap = MAP_WIDTH;
    protected $_widthSprite = 32;

    public $texturies = array (
        self::MOUNTAIN => 'mountain',
        self::WATER => 'water',
        self::FOREST => 'forest',
        self::EARTH => 'earth',
        self::SAND => 'sand',
        self::BOG => 'bog',
    );

    public $units = array (
        self::BASE => 'base',
        self::BASE_TEAM_1 => 'base-1',
        self::BASE_TEAM_2 => 'base-2',
        self::INFANTRY => 'infantry',
        self::INFANTRY_TEAM_1 => 'infantry-1',
        self::INFANTRY_TEAM_2 => 'infantry-2',
        self::TANKS => 'tanks',
        self::TANKS_TEAM_1 => 'tanks-1',
        self::TANKS_TEAM_2 => 'tanks-2',
        self::WARPLANES => 'warplanes',
        self::WARPLANES_TEAM_1 => 'warplanes-1',
        self::WARPLANES_TEAM_2 => 'warplanes-2',
    );

    public $unitsList = array(
        array(self::BASE_TEAM_1, 1, 1),
        array(self::BASE_TEAM_2, MAP_HEIGHT - 2, MAP_WIDTH - 2),
        array(self::INFANTRY_TEAM_1, 2, 4),
        array(self::INFANTRY_TEAM_2, MAP_HEIGHT - 3, MAP_WIDTH - 5),
        array(self::INFANTRY_TEAM_1, 3, 3),
        array(self::INFANTRY_TEAM_2, MAP_HEIGHT - 4, MAP_WIDTH - 4),
        array(self::INFANTRY_TEAM_1, 4, 2),
        array(self::INFANTRY_TEAM_2, MAP_HEIGHT - 5, MAP_WIDTH - 3),
        array(self::TANKS_TEAM_1, 4, 0),
        array(self::TANKS_TEAM_2, MAP_HEIGHT - 5, MAP_WIDTH - 1),
        array(self::TANKS_TEAM_1, 0, 4),
        array(self::TANKS_TEAM_2, MAP_HEIGHT - 1, MAP_WIDTH - 5),
        array(self::WARPLANES_TEAM_1, 2, 2),
        array(self::WARPLANES_TEAM_2, MAP_HEIGHT - 3, MAP_WIDTH - 3),
    );

}