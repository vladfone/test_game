<?php
/**
 * Created by Vlad Doroshchuk.
 */

/** Route class call the action method of the controller*/
class Route {
    public static function init(){
        $controller = 'map';
        $action = 'index';
        if ( isset ($_GET['view'])){
            $url = rtrim($_GET['view'], '/');
            $url = explode('/', $url);

            if (is_array($url)){
                $controller = $url[0];
                if (isset($url[1]))
                    $action = $url[1];

            }
        }

        if (file_exists($_SERVER['DOCUMENT_ROOT']."/app/controllers/".ucfirst($controller)."Controller.php"))
            require_once $_SERVER['DOCUMENT_ROOT']."/app/controllers/".ucfirst($controller)."Controller.php";
        else {
            echo "file ".$controller."Controller.php doesn`t exist";
        }
        if (class_exists($controller."Controller")){
            $controllerName = ucfirst($controller)."Controller";
            $controllerInst = new $controllerName($controller, $action);
            if (method_exists($controllerInst, $action)){
                $controllerInst->$action();
            }else{
                echo "Action $action doesn`t exist";
            }
        } else{
            echo "Controller $controller doesn`t exist";
        }
    }
}

