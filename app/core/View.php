<?php
/**
 * Created by Vlad Doroshchuk.
 */
/** main View
$layout - layout View file name
$controller - controller name
$includes - array of the includes (css, js)
 */
class View {
    protected $layout = "layout.view.php";
    protected $controller;
    protected $includes;

    public function __construct ($model=array(), $includes = array(), $controller){
        $this->controller = $controller;
        $this->includes = $includes;
    }
    /** load js scripts */
    protected function loadScripts($js){
        $scripts = '';
        if (is_array($js))
            foreach ($js as $jsEach)
                $scripts .= '<script src="http://'.$_SERVER['HTTP_HOST'].'/includes/js/'.$jsEach.'"></script>';
        return $scripts;
    }
    /** load css styles */
    protected function loadStyles($css){
        $styles = '';

        if (is_array($css))
            foreach ($css as $cssEach)
                $styles .= '<link href="http://'.$_SERVER['HTTP_HOST'].'/includes/css/'.$cssEach.'" rel="stylesheet">';

        return $styles;
    }

    public function render($attr = null, $view = null){
        if ($view != null) $this->controller = $view;
        extract($this->includes);
        $viewPath = $_SERVER['DOCUMENT_ROOT']."/app/views/".$this->controller.'.view.php';
        require_once $_SERVER['DOCUMENT_ROOT']."/app/views/".$this->layout;
    }
}