<?php
/**
 * Created by Vlad Doroshchuk.
 */

require_once $_SERVER['DOCUMENT_ROOT']."/app/core/Model.php";
require_once $_SERVER['DOCUMENT_ROOT']."/app/models/UnitsModel.php";

class BaseModel extends UnitsModel {

    public $_canNotBeAt = array(
        self::MOUNTAIN,
        self::WATER,
        self::FOREST,
        self::SAND,
        self::BOG,
    );

}