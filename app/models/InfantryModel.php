<?php
/**
 * Created by Vlad Doroshchuk.
 */

require_once $_SERVER['DOCUMENT_ROOT']."/app/core/Model.php";
require_once $_SERVER['DOCUMENT_ROOT']."/app/models/UnitsModel.php";

class InfantryModel extends UnitsModel {

    public $_canNotBeAt = array(
        self::BOG
    );

    public $_canFightWith = array(
        self::INFANTRY,
        self::TANKS,
    );
}