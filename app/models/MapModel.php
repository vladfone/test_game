<?php
/**
 * Created by Vlad Doroshchuk.
 *
 */

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/core/Model.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/app/models/CreateUnitsModel.php";

class MapModel extends Model
{

    /**
     * generate map
     */
    public function generateMap()
    {
        for ($y = 0; $y < $this->_heightMap; $y++) {
            for ($x = 0; $x < $this->_widthMap; $x++) {
                $this->map[$y][$x] = self::EARTH;
            }
        }
        $this->setElementsToMap();
        return $this->map;
    }

    /**
     * get width
     */
    public function widthMap($addString = '')
    {
        return $this->_widthMap * $this->_widthSprite . $addString;
    }

    /**
     * algorithm creation map
     */
    protected function setElementsToMap()
    {
        for ($i = 0; $i <= MAP_NUMBER_ELEMENTS; $i++) {
            $startX = rand(1, $this->_heightMap - 5);
            $startY = rand(1, $this->_widthMap - 5);
            $endX = rand($startX, $this->_heightMap - 2);
            $endY = rand($startY, $this->_widthMap - 2);
            $this->addElementsMap(rand(1, 6), $startX, $startY, $endX, $endY);
        }
        $this->placeForBases();
        $this->setUnits();
    }

    protected function addElementsMap($texturieType = self::WATER, $startX = 0, $startY = 0, $endX = 5, $endY = 10)
    {
        $numberElements = (($endX - $startX) * ($endY - $startY));
        /**
         * approximately center of block
         */
        $centerY = floor(($endX - $startX) / 2) + $startX;
        $centerX = floor(($endY - $startY) / 2) + $startY;
        $map[$centerY][$centerX] = $texturieType;
        $i = 0;
        do {
            foreach ($map as $key => $elemMap) {
                $addElementX = $key + rand(-1, 1);
                $addElementY = key($elemMap) + rand(-1, 1);
                if ($addElementY >= $startY && $addElementX >= $startX && $addElementY <= $endY && $addElementX <= $endX &&
                    $addElementX > 0 && $addElementY > 0 && $addElementX < $this->_heightMap && $addElementY < $this->_widthMap
                ) {
                    $map[$addElementY][$addElementX] = $this->map[$addElementY][$addElementX] = $texturieType;
                    $i++;
                }
            }
        } while ($i < $numberElements);
    }

    /**
     * set units
     */
    public function setUnits()
    {
        foreach ($this->unitsList as $unit) {
            $this->checkPositionUnits($unit);
        }

    }

    /**
     * check position units
     */
    protected function checkPositionUnits($unit)
    {
        $unitTmp = CreateUnitsModel::build(str_replace(array('-1', '-2'), '', $this->units[$unit[0]]), $unit[1], $unit[2], $unit[0], $this->map);
        if(empty($unitTmp->getPositionX()) && empty($unitTmp->getPositionY())) {
            $newCoordinates = $this->findOtherPositionUnit($unitTmp);
            $unitTmp->setPositionY($newCoordinates['Y']);
            $unitTmp->setPositionX($newCoordinates['X']);
            $unit[1] = $newCoordinates['Y'];
            $unit[2] = $newCoordinates['X'];
            $unitTmp = CreateUnitsModel::build(str_replace(array('-1', '-2'), '', $this->units[$unit[0]]), $unit[1], $unit[2], $unit[0], $this->map);
        }
        $this->unit[$unit[1]][$unit[2]] = $unitTmp;
        var_dump($unitTmp);
    }

    /**
     * find position units
     */
    protected function findOtherPositionUnit($unit)
    {
        /**
         * TODO calculate and check new position
         */
        $result = array('Y' => rand(MAP_HEIGHT-5, MAP_HEIGHT-1), 'X' => rand(1, 5));
        return $result;
    }

    /**
     * clearing plases for bases
     */
    public function placeForBases()
    {
        for ($y = 0; $y < self::PLACE_FOR_BASES; $y++)
            for ($x = 0; $x < self::PLACE_FOR_BASES; $x++)
                $this->map[$y][$x] = $this->map[$this->_heightMap - $y - 1][$this->_widthMap - $x - 1] = self::EARTH;
    }
}