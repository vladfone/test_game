<?php
/**
 * Created by Vlad Doroshchuk.
 *
 */

require_once $_SERVER['DOCUMENT_ROOT']."/app/core/Model.php";

class UnitsModel extends Model {

    protected $_strength = 100;
    protected $_paces = 2;
    protected $_positionY;
    protected $_positionX;
    protected $_unitTypeAndTeam;
    protected $_canNotBeAt = array();
    protected $_canFightWith = array();


    public function __construct ($positionY, $positionX, $unitTypeAndTeam, $map = array()) {
        if (!empty($map[$positionY][$positionX]) && !in_array($map[$positionY][$positionX], $this->_canNotBeAt)) {
            $this->_positionY = $positionY;
            $this->_positionX = $positionX;
            $this->_unitTypeAndTeam = $unitTypeAndTeam;
            $this->map = $map;
        } else {
            return false;
        }
    }

    /**
     * @param $direction - direction of moving units
     *
     * check can unit move or not and if unit cam move change position in right direction
     * also if unit move to position of other unit check can unit fight or not
     *
     */

    public function move($direction = false, $units = array()) {
        switch ($direction) {
            case self::DIRECTION_LEFT:
                if($this->_positionX >= 1 && !in_array($this->map[$this->_positionY][$this->_positionX - 1], $this->_canNotBeAt)) {
                    if($this->checkFight($this->_positionY, $this->_positionX - 1, $units))
                        $this->_positionX--;
                }
                break;
            case self::DIRECTION_RIGHT:
                if($this->_positionX < MAP_HEIGHT && !in_array($this->map[$this->_positionY][$this->_positionX + 1], $this->_canNotBeAt)) {
                    if($this->checkFight($this->_positionY, $this->_positionX + 1, $units))
                        $this->_positionX++;
                }
                break;
            case self::DIRECTION_UP:
                if($this->_positionY >= 1 && !in_array($this->map[$this->_positionY - 1][$this->_positionX], $this->_canNotBeAt)) {
                    if($this->checkFight($this->_positionY - 1, $this->_positionX, $units))
                        $this->_positionY--;
                }
                break;
            case self::DIRECTION_DOWN:
                if($this->_positionY < MAP_WIDTH && !in_array($this->map[$this->_positionY + 1][$this->_positionX], $this->_canNotBeAt)) {
                    if($this->checkFight($this->_positionY + 1, $this->_positionX, $units))
                        $this->_positionY--;
                }
                break;
        }
    }

    /**
     * @param $positionY - coordinate y unit
     * @param $positionX - coordinate x unit
     * @param $units array of units objects
     * @return bool - true - can fight with unit
     */

    public function checkFight($positionY, $positionX, $units) {
        if(!empty($units[$positionY][$positionX])) {
            if (in_array($units[$positionY][$positionX]->getUnitTypeAndTeam(), $this->_canFightWith))
                $this->fightWIth($positionY, $positionX, $units[$positionY][$positionX]);
            else
                return false;
        }
        return true;
    }

    /**
     * @param $positionY  - coordinate y unit
     * @param $positionX- coordinate x unit
     * @param $unit - unit fight with
     * @return bool - true - win
     */
    public function fightWIth($positionY, $positionX, $unit) {
        /**
         *  TODO algorithm fight
         */
        return true;
    }

    /**
     * @return mixed - unitTypeAndTeam
     */
    public function getUnitTypeAndTeam() {
        return $this->units[$this->_unitTypeAndTeam];
    }

    /**
     * @return mixed - coordinate y unit
     */
    public function getPositionY() {
        return $this->_positionY;
    }
    /**
     * @return mixed - coordinate x unit
     */
    public function getPositionX() {
        return $this->_positionX;
    }

    /**
     * @param $y - new y coordinate unit
     * set coordinate unit
     */
    public function setPositionY($y) {
        $this->_positionY = $y;
    }

    /**
     * @param $x - new x coordinate unit
     * set coordinate unit
     */
    public function setPositionX($x) {
        $this->_positionX = $x;
    }

    /**
     * @return string with list textures on which
     */
    public function getCanNotBeAt() {
        $result = '--';
        foreach ($this->_canNotBeAt as $canNotBeAt)
            $result .= $this->texturies[$canNotBeAt] . '-';
        return $result;
    }

}