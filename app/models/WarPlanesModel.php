<?php
/**
 * Created by Vlad Doroshchuk.
 */

require_once $_SERVER['DOCUMENT_ROOT']."/app/core/Model.php";
require_once $_SERVER['DOCUMENT_ROOT']."/app/models/UnitsModel.php";

class WarPlanesModel extends UnitsModel {

    public $_canFight = array(
        self::BOG
    );

    public $_canFightWith = array(
        self::TANKS,
    );

}