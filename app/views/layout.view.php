<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Blog</title>
        <?php echo $this->loadStyles($css);?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>
    <body>
    <header>
        <h4><a href="/">Тестовая работа для Together Networks </a></h4>
    </header>

    <div class="wrap">
        <hr>
        <?php include $viewPath;?>
    </div>

    <hr>
    <footer>
        При любом использовании материалов сайта ссылка на сайт <?php print_r($_SERVER['HTTP_HOST']); ?> обязательна.
    </footer >
    <?php echo $this->loadScripts($js);?>
    </body>
</html>