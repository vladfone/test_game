<div class="map">
    <?php if(isset($attr['model'])):?>
    <?php //var_dump($attr['model']->generateMap()); die()?>
        <?php foreach ($attr['model']->generateMap() as $_key => $_map): ?>
            <div class="map-row" style="width:<?= $attr['model']->widthMap('px');?>">
                <?php foreach ($_map as $__key => $__map): ?>
                    <img id="map-<?=$_key?>-<?=$__key?>" class="texturies <?=$attr['model']->texturies[$__map]?> <?= !empty($attr['model']->unit[$_key][$__key])? $attr['model']->unit[$_key][$__key]->getUnitTypeAndTeam() . " " . $attr['model']->unit[$_key][$__key]->getCanNotBeAt():"";?>">
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    <?php endif;?>
</div>
